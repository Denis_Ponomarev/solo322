// берём Express
var express = require('express');
// создаём Express-приложение
var app = express();
var port = process.env.PROT || 8080;

app.get('/', function (req, res) {
    var path = require('path');
    res.sendFile(path.resolve('../index.html'));
});



// запускаем сервер на порту 8080
app.listen(8080);
// отправляем сообщение
console.log('Сервер стартовал!'+ port);